﻿using System;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;

namespace Space_Project.Enemy
{
    public class EnemyBiological : IEnemy
    {


        public override void performAttack() {
            atk_behavior.Attack();
        }

        public override void performMove() {
            mov_behavior.Move();
        }
        public void SetAi(EnemyBiological _EnemyBiological)
        {
            m_EnemyBiological = _EnemyBiological;
        }
        public override bool OnCollisionPlayer()
        {
            //確認撞到玩家
            return ColPlayerFlag;
        }


    }
    
}