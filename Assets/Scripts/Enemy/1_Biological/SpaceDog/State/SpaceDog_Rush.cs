﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace StateMachine
{
    public class SpaceDog_Rush :IState
{
        public override void StateBegin()
        {
        }
        public override void StateUpdate()
        {
             m_StateController.Enemy.performMove();
            if (m_StateController.Enemy.OnCollisionPlayer())
               m_StateController.ChangeState(new SpaceDog_Explosion());
        }
        public override void StateExit()
        {
        }
    }
}
