﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace StateMachine
{
    public class SpaceDog_Explosion : IState
    {
        public override void StateBegin()
        {
        }
        public override void StateUpdate()
        {
            m_StateController.Enemy.performAttack();
            
        }
        public override void StateExit()
        {
        }
    }
}
