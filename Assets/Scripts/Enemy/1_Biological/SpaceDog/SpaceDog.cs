﻿using System.Collections;
using System.Collections.Generic;
using Space_Project.Enemy;
using Space_Project.Enemy.Attack;
using Space_Project.Enemy.Move;
using UnityEngine;
using StateMachine;


public class SpaceDog : EnemyBiological
{

    SpaceDog m_SpaceDog;

    void Awake()
    {
        m_SpaceDog = this;
        m_stateController.SetStartState(new SpaceDog_Idle());
     }
 
    // Start is called before the first frame update
    void Start()
    {
         //初始化攻擊模式
        atk_behavior = new AttackExplosion();
        //初始化移動模式
        mov_behavior = new MoveTrace();
        //初始化狀態
               
    }


    // Update is called once per frame
    void Update()
    {
        m_stateController.SetAi(m_SpaceDog);
        //狀態更新
        m_stateController.StateUpdate();
        


    }

}

