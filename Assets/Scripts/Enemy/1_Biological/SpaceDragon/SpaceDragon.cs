﻿using System.Collections;
using System.Collections.Generic;
using Space_Project.Enemy;
using Space_Project.Enemy.Attack;
using Space_Project.Enemy.Move;
using UnityEngine;

public class SpaceDragon :EnemyBiological 
{

    SpaceDragon m_SpaceDragon;


    void Awake()
    {
        m_SpaceDragon = this;
        m_stateController.SetStartState(new SpaceDragon_Idle());
    }

    void Start()
    {
        //初始化攻擊模式
        atk_behavior = new AttackFireBall();
    }


    // Update is called once per frame
    void Update()
    {
        m_stateController.SetAi(m_SpaceDragon);

        //狀態更新
        m_stateController.StateUpdate();
    }
    
}
