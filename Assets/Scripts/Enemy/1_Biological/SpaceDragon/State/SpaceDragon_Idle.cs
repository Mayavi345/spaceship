﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Space_Project.Enemy;
using Space_Project.Enemy.Attack;
using Space_Project.Enemy.Move;
using StateMachine;

public class SpaceDragon_Idle:IState
{

    Vector3 EnemyPosioin;
    Vector3 ShipPosition;
    public override void StateBegin()
    {
        EnemyPosioin = m_StateController.Enemy.EnemyPosition;
        ShipPosition = GameManager.Instance.ship.Position;
    }
    public override void StateUpdate()
    {

        if (m_StateController.Enemy.WarningRange(ShipPosition, EnemyPosioin, 5))
            m_StateController.ChangeState(new SpaceDragon_Attack());

    }
    public override void StateExit()
    {
    }
}
