﻿using System;
using System.Collections.Generic;
using UnityEngine;
namespace Space_Project.Enemy.Move
{
    public interface IMoveBehavior
    {
        void Move();
    }
}
