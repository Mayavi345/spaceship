﻿using System;
using System.Collections.Generic;
using Space_Project.Enemy.Attack;
using Space_Project.Enemy.Move;
using StateMachine;
using UnityEngine;

namespace Space_Project.Enemy
{
    public abstract class IEnemy: MonoBehaviour
    {
        public Vector3 EnemyPosition;

        public StateController m_stateController = new StateController();
        protected EnemyBiological m_EnemyBiological = null;
        public IAttackBehavior atk_behavior;
        public IMoveBehavior mov_behavior;

        public bool ColPlayerFlag = false;
        public bool WarningFlag = false;

        public abstract bool OnCollisionPlayer();
        public abstract void performAttack();
        public abstract void performMove();
    

        public bool WarningRange(Vector3 SpaceShipPosition, Vector3 target, float AttackRange)
        {
            //計算是否進入範圍
            if (Vector3.Distance(SpaceShipPosition, target) < AttackRange)
            {
                WarningFlag = true;
                return true;
            }
            else return false;

        }
    }
}
