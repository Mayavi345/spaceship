﻿using System.Collections;
using System.Collections.Generic;
using Space_Project.Enemy.Attack;
using UnityEngine;

public class AttackDecreaseSpeed : IAttackBehavior
{
    public void Attack()
    {
        GameManager.Instance.ship.DecreaseSpeed();
        Debug.Log("緩速");

    }
}
