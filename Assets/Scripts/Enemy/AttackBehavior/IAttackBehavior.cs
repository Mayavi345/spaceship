﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Space_Project;


namespace Space_Project.Enemy.Attack
{

    public interface IAttackBehavior
    {
        void Attack();
    }
}
