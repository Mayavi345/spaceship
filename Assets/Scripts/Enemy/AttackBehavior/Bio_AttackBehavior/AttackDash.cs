﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Space_Project.Enemy.Attack
{
    class AttackDash : IAttackBehavior
    {
        public void Attack()
        {
            GameManager.Instance.ship.GetHurt();
            Debug.Log("Attack Dash");
        }
    }
}
