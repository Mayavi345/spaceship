﻿using System.Collections;
using System.Collections.Generic;
using Space_Project.Enemy;
using UnityEngine;

public class EnemyColObject : IEnemy
{

    public override void performAttack()
    {
        if (atk_behavior == null)
            return;
        atk_behavior.Attack();
    }
   

    public override void performMove()
    {
        if (mov_behavior == null)
            return;
        mov_behavior.Move();
    }
    public void SetAi(EnemyBiological _EnemyBiological)
    {
        m_EnemyBiological = _EnemyBiological;
    }

    public override bool OnCollisionPlayer()
    {
        //確認撞到玩家
        return ColPlayerFlag;
    }

    
}
