﻿using System.Collections;
using System.Collections.Generic;
using Space_Project.Enemy.Attack;
using Space_Project.Enemy.Move;
using UnityEngine;

public class StoneIceMagnet : EnemyColObject
{
    StoneIceMagnet m_StoneNormal;
    public IAttackBehavior atk_behavior2;


    void Awake()
    {
        m_StoneNormal = this;
        m_stateController.SetStartState(new StoneIceMagnet_Idle());
    }

    void Start()
    {
        //初始化移動
        mov_behavior = new MoveTrace();
        //初始化攻擊模式
        atk_behavior = new AttackHit();
        atk_behavior2 = new AttackDecreaseSpeed();
    }


    // Update is called once per frame
    void Update()
    {
        m_stateController.SetAi(m_StoneNormal);
        //狀態更新
        m_stateController.StateUpdate();
    }
    public override void performAttack()
    {
        atk_behavior.Attack();
        atk_behavior2.Attack();

    }
}
