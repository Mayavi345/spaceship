﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Space_Project.Enemy;
using Space_Project.Enemy.Attack;
using Space_Project.Enemy.Move;
using StateMachine;


public class StoneNormal_Idle : IState
{
    public override void StateBegin()
    {

    }
    public override void StateUpdate()
    {
        m_StateController.Enemy.performMove();

        if (m_StateController.Enemy.ColPlayerFlag)
            m_StateController.Enemy.performAttack();
        

    }
    public override void StateExit()
    {
    }
}
