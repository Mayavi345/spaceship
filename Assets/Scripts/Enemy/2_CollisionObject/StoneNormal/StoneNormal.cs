﻿using System.Collections;
using System.Collections.Generic;
using Space_Project.Enemy.Attack;
using Space_Project.Enemy.Move;
using UnityEngine;

public class StoneNormal : EnemyColObject
{
    StoneNormal m_StoneNormal;
 
  
    void Awake()
    {
        m_StoneNormal = this;
        m_stateController.SetStartState(new StoneNormal_Idle());
    }

    void Start()
    {
        //初始化移動
       mov_behavior = new MoveFloat();
        //初始化攻擊模式
       atk_behavior = new AttackHit();
     }


    // Update is called once per frame
    void Update()
    {
        m_stateController.SetAi(m_StoneNormal);
        //狀態更新
        m_stateController.StateUpdate();
    }
   
}
