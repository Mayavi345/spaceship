﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Space_Project.Enemy;

namespace StateMachine
{
    public abstract class IState
    {
        public StateController m_StateController;
        public StateController Controller {
            get {
                return m_StateController;
            }
        }
 
        public void SetProperty(StateController oController)
        {
            m_StateController = oController;
        }
      
        public virtual void StateBegin()
        {
        }
        public virtual void StateUpdate()
        {
        }
        public virtual void StateExit()
        {
        }
    }
}