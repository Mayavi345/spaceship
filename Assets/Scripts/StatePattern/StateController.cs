﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Space_Project.Enemy;

namespace StateMachine
{
    public class StateController
    {
        public IState m_State;
        public IState CurrenState => m_State;
        public IEnemy m_Enemy;
        public IEnemy Enemy => m_Enemy;
        public StateController() { }
        public StateController(IState oStartState) :this(){
            SetStartState(oStartState);
        }
        public void SetAi(IEnemy _Enemy)
        {
            m_Enemy = _Enemy;
            
        }

        public void SetStartState(IState state)
        {
            m_State = state;
            m_State.SetProperty(this);
    
        }
         
        public void StateUpdate()
        {
              m_State.StateUpdate();
        }
        public void ChangeState(IState state)
        {
            //更換狀態
            m_State = state;
            m_State.SetProperty(this);

        }
        public void GetCurrenState()
        {

        }
    }
}
